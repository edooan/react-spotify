import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Link, Route, browserHistory, hashHistory, IndexRoute} from 'react-router';
import App from './components/App';
import Home from './components/Home';
import Profile from './components/Profile';

ReactDOM.render((
   <Router history = {browserHistory}>
      <Route path = '/' component = {App}>
         <IndexRoute component = {Home} />
         <Route path = 'home' component = {Home} />
         <Route path = 'profile' component = {Profile} />
      </Route>
   </Router>
  
), document.getElementById('app'))