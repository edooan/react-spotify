import React from 'react';

var client_id = 'ea93305a20194c7482f3bc7b76707ac4';

export default class Home extends React.Component {
  render() {
    return (
      <div className="home" style={{marginTop: 300}}>
        <div className="row column text-center">
          <h1>Welcome! to Spotify Player with ReactJS</h1>
          <p>Please login into Spotify to see your data.</p>
          <p>
            <a href={"https://accounts.spotify.com/authorize/?client_id=" + client_id + "&response_type=token&redirect_uri=http%3A%2F%2Flocalhost%3A8080%2Fprofile&scope=user-read-private%20user-read-email&state=34fFs29kd09"} className="success button">LOG IN WITH SPOTIFY</a>
          </p>
        </div>
      </div>
    );
  }
}
