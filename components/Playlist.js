import React from 'react';
import Player from './Player';


export default class Playlist extends React.Component {
  render() {
    return (
      <div className="column">
        <h6>{this.props.playlist.name} / {this.props.playlist.tracks.total} songs</h6>
        <Player uri={this.props.playlist.uri} />
        <hr />
      </div>
    );
  }
}
