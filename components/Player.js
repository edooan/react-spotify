import React from 'react';

export default class Player extends React.Component {
  render() {
    return (
        <iframe src={"https://embed.spotify.com/?uri="+ this.props.uri +"&theme=white"} width="100%" height="300" frameBorder="0" allowTransparency="true"></iframe>
    );
  }
}
