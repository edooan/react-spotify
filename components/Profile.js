import React from 'react';
import axios from 'axios';
import { Link } from 'react-router';
import Playlist from './Playlist';

export default class Profile extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      id: '',
      country :'',
      name:'',
      email:'',
      product:'',
      image: '',
      playlists: [],
    }
  }

  componentDidMount() {
    
    function getHashParams() {
      let hashParams = {};
      let e, r = /([^&;=]+)=?([^&;]*)/g,
          q = window.location.hash.substring(1);
      while ( e = r.exec(q)) {
         hashParams[e[1]] = decodeURIComponent(e[2]);
      }
      return hashParams;
    }

    let params = getHashParams();

    let access_token = params.access_token;

    axios({
      method: 'get',
      url: 'https://api.spotify.com/v1/me',
      headers: {
        'Authorization': 'Bearer ' + access_token
      }
    }).then( res => {
      this.setState({
        country: res.data.country,
        name: res.data.display_name,
        email: res.data.email, 
        product: res.data.product,
        image: res.data.images[0].url
      });
    });

    axios({
      method: 'get',
      url: 'https://api.spotify.com/v1/me/playlists',
      headers: {
        'Authorization': 'Bearer ' + access_token
      }
    }).then(res => {
      let playlists = res.data.items;
      this.setState({ playlists })
    });


  }

  render() {
    return (
      <div>
        <div className="off-canvas position-left reveal-for-large" id="my-info" data-off-canvas="" data-position="left" aria-hidden="true" data-offcanvas="lyzd1a-offcanvas">
          <div className="row column">
            <br />
            <img className="thumbnail" src={this.state.image} />
            <h5>{this.state.name}</h5>
            <h5>{this.state.email}</h5>
            <Link to='home'>Logout</Link>
          </div>
        </div>
        <div className="off-canvas-content" data-off-canvas-content="">
          <div className="row small-up-2 medium-up-3 large-up-4">
            <h4>Public Playlists</h4>
            {this.state.playlists.map(playlist => (
              <Playlist key={playlist.id} playlist={playlist}/>
            ))}
          </div>
          <div className="js-off-canvas-exit"></div>
        </div>
      </div>
    );
  }
}
